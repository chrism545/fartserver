'use strict';

const os = require('os');

module.exports = async (app) => {

    console.log(`OS type: ${os.type()}`); // "Windows_NT"
    console.log(`OS release: ${os.release()}`);// "10.0.14393"
    console.log(`OS platform: ${os.platform()}`);// "win32"

    const { Sound } = app.models;

    const defualtSound = await Sound.findOne({where: { file: 'fart-01.wav'}});

    if (!defualtSound) {
        const d = await Sound.create({
            name: 'Fart 01',
            file: 'fart-01.wav',
            size: 9999
        });

        console.log(`Created defaut sound ${JSON.stringify(d, null, 2)}`);
    }


};
