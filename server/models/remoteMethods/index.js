'use strict';

/**
 * @description Loads sub modules by searching child folders for an index file,
 * if no index file is found then it loads all module files in a folder, passing
 * in this Model object
 *
 * @author Dan Haber <dan.haber@evus.com>
 *
 * @param {Class} Self - Pass LoopBack Model
 *
*/
const fs = require('fs');
const path = require('path');

const thisFolder = __dirname;

const isDirectory = (dir) => {
    return fs.lstatSync(dir).isDirectory();
};

const getDirectories = (dir) => {
    return fs.readdirSync(dir)
        .map((name) => {
            return path.join(dir, name);
        })
        .filter(isDirectory);
};

// Get the index.js file in this directory
const getDirIndex = (dir) => {
    fs.readdirSync(dir)
        .forEach((file) => {
            if (file === 'index.js') {
                return file;
            }
        });

    return false;
};

// Loop through the model files and apply each to Self
const gatherModelMethods = (Self, folder) => {

    fs.readdirSync(folder)
    .forEach((file) => {
        if (file.endsWith('.js') && !file.endsWith('index.js') ) {
            // eslint-disable-next-line global-require, import/no-dynamic-require
            const methods = require(`${folder}/${file}`);
            methods(Self);
        }
    });


    // // Get all directories in this folder.
    // const dirs = getDirectories(folder);

    // for (let i = 0; i < dirs.length; i += 1) {
    //     const dir = dirs[i];
    //     // first find the index.js file
    //     const index = getDirIndex(dir);

    //     // if index load that
    //     if (index) {
    //         // eslint-disable-next-line global-require, import/no-dynamic-require
    //         const method = require(`${dir}/${index}`);
    //         method(Self);
    //     } else { // else load all .js files
    //         fs.readdirSync(dir)
    //             .forEach((file) => {
    //                 if (file.endsWith('.js')) {
    //                     // eslint-disable-next-line global-require, import/no-dynamic-require
    //                     const methods = require(`${dir}/${file}`);
    //                     methods(Self);
    //                 }
    //             });
    //     }
    // }
};

module.exports = (Self) => {
    gatherModelMethods(Self, thisFolder);
};
