'use strict';

const spawn = require('child_process').spawn;
const os = require('os');
const rootPath = './uploads/sounds/';

module.exports = (Public) => {

    function playSound(file) {
        const playerList = {
            darwin: 'afplay',
            linux: 'aplay',
            win32: 'play'
        };

        const command = [file];
        const player = playerList[os.platform()];

        if (!player) {
            return Promise.reject('oops no player for platform: ' + os.platform());
        }

        console.log('playing => ' + file);

        const process = spawn(player, command, {
            detached: true,
            stdio: 'ignore'
        });

        // process.stderr.setEncoding('ascii');

        let timedOut = false;

        const timerId = setTimeout(() => {
            timedOut = true;
            process.kill();
        }, 5000); // 5 second timout

        console.log('playing is prommised');

        return new Promise((resolve, reject) => {
            process.on('exit', function (code, signal) {

                console.log(`exit => ${file} code: ${code} signal: ${signal}`);

                if (timedOut) {
                    reject(file + ' ran for too long');
                } else {
                    clearTimeout(timerId);

                    if (code == null || signal != null || code === 1) {
                        reject('couldnt play, had an error ' + '[code: ' + code + '] ' + '[signal: ' + signal + ']');
                    } else if (code == 2) {
                        reject(file + '=> could not be read by your player.');
                    } else {
                        resolve('completed => ' + file);
                    }
                }
            });
        });

    }

    Public.remoteMethod(
        'playSound',
        {
            http: { path: '/playSound/:id', verb: 'get' },
            accepts: [
                { arg: 'id', type: 'number', required: true }
            ],
            returns: { arg: 'result', type: 'Sound', root: true },
        }
    );

    Public.playSound = async (id) => {

        const { models } = Public.app;

        const sound = await models.Sound.findById(id);

        if (!sound)
            throw new Error(`Sound ${id} not found`);

        sound.timesPlayed += 1;

        await playSound(rootPath + sound.file);

        return sound.save();
    };

};
