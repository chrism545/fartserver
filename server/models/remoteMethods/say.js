'use strict';

const spawn = require('child_process').spawn;
const os = require('os');

module.exports = (Public) => {

    function say(text, options) {
        const playerList = {
            darwin: 'say',
            linux: 'espeak'
        };

        const command = options ? [options, text] : [text];
        const player = playerList[os.platform()];

        if (!player) {
            return Promise.reject('oops no "say" for platform: ' + os.platform());
        }

        console.log(`Saying ${options || ''} ${text}`);
        const process = spawn(player, command);

        return new Promise((resolve, reject) => {
            process.on('exit', function (code, signal) {
                if (code) {
                    reject('couldnt Say, had an error ' + '[code: ' + code + '] ' + '[signal: ' + signal + ']');
                } else {
                    resolve(`Said ${options || ''} ${text}`);
                }
            });
            process.on('error', reject);

        });

    }

    Public.remoteMethod(
        'say',
        {
            http: { path: '/say', verb: 'get' },
            accepts: [
                { arg: 'text', type: 'string', required: true },
                { arg: 'options', type: 'string'}
            ],
            returns: { arg: 'result', type: 'string' },
        }
    );

    Public.say = async (text, options) => {
        return say(text, options);
    };

};
