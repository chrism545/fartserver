'use strict';

module.exports = (Public) => {

    Public.remoteMethod(
        'listSounds',
        {
            http: { path: '/listSounds', verb: 'get' },
            returns: {arg: 'result', type: 'string', root: true},
        }
    );

    Public.listSounds = async () => {
        const { models } = Public.app;
        return models.Sound.find();
    };

};
