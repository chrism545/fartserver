'use strict';

module.exports = (Public) => {

    Public.remoteMethod(
        'uploadSound',
        {
            http: { path: '/uploadSound', verb: 'post' },
            accepts: [
                { arg: 'req', type: 'object', 'http': { source: 'req' } },
                { arg: 'res', type: 'object', 'http': { source: 'res' } },
                { arg: 'name', type: 'string', 'http': { source: 'query' } }
            ],
            returns: { arg: 'result', type: 'string' },
        }
    );

    Public.uploadSound = async (req, res, name) => {
        const { models } = Public.app;

        const container = {
            //root: './upload',
            // acl: 'public-read',
            container: 'sounds',
            allowedContentTypes: ['audio/vnd.wav', 'audio/wav'],
            maxFileSize: 10 * 1024 * 1024
        };

        const upload = await new Promise((resolve, reject) => {
            models.FileStorage.upload(req, res, container, (err, file) => {
                if (err) return reject(err);
                else resolve(file);
            });
        });

        // upload = {
        //     'files': {
        //        'file': [{ 'container': 'sounds', 'name': 'msg0001.WAV', 'type': 'audio/wav', 'field': 'sampleFile', 'size': 58170 }]
        //     },
        //     'fields': { }
        // };

        // validate result
        if(!upload || !upload.files || !upload.files.file || upload.files.file.length != 1) {
            throw new Error('Upload requiers a file field');
        }

        // get relevant data
        const file = upload.files.file[0];
        const soundName = name || file.name;

        const soundData = {
            name: soundName,
            file: file.name,
            size: file.size
        };

        const soundObject = await models.Sound.create(soundData);

        return soundObject;
    };


};
