'use strict';

const remoteMethods = require('./remoteMethods');

module.exports = (Public) => {
    remoteMethods(Public);
};
