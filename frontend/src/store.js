import { EventEmitter } from 'events'
import { Promise } from 'es6-promise'
import axios from 'axios'

const apiURL = 'http://' + window.location.hostname + ':3000/api/'
const returnData = (response) => { return response.data; };

const store = new EventEmitter()

export default store

store.apiURL = apiURL;

store.fetchSoundsList = () => {
    return axios.get(apiURL + 'Public/listSounds').then(returnData);
}

store.playSound = (id) => {
    return axios.get(apiURL + 'Public/playSound/' + id).then(returnData);
}

store.say = (text) => {
  return axios.get(apiURL + 'Public/say/', {
    params: {
      text: text
    }
  }).then(returnData);
}
